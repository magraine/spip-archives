<?php

namespace Spip\Tools\Archives\Command;

use DateTime;
use Spip\Tools\Archives\ArchiveException;


class ArchiveCheckout extends ArchiveBase {

    protected static $defaultName = 'archive:checkout';
    protected static $defaultDescription = 'Génère une archive d’un SPIP (avec Checkout)';

    protected $componentDir = 'checkout/';

    protected function verify(string $version): bool {
        $source = $this->getSourceDir($version);
        $cmd = "cd $source && git rev-parse --verify %s^{} 2>/dev/null";
        $hash_head = exec(sprintf($cmd, 'HEAD'));
        $hash_version = exec(sprintf($cmd, $version));
        return $hash_head === $hash_version;
    }

    protected function download(string $version, bool $noDownload = false, bool $interactive = true): bool {
        $source = $this->getSourceDir($version);
        $this->io->section("Checkout $version in $source");
        if ($noDownload) {
            if (is_file($source . '.git/config')) {
                $this->io->text("! <comment>Checkout ignoré.</comment>");
                return $this->verify($version);
            }
        }

        $this->createDirectory(dirname($source));
        $cmd = "checkout spip -b'$version' '" . rtrim($source, '/') . "'";
        passthru($cmd);
        return $this->verify($version);
    }

    protected function build(string $version, bool $interactive = true) {
        $destination = $this->getBuildDir($version);
        $source = $this->getSourceDir($version);

        $this->io->section("Build $version in $destination");
        $this->createDirectory($destination);
        $this->createDirectory($source);

        $params = $interactive ? '' : ' --no-interaction';
        $composer_file = $source . 'composer.json';
        if (!file_exists($composer_file)) {
            $template = $this->getPathTemplateComposer();
            $this->copyTemplate($template, $composer_file, $version, $interactive);
    
            $cmd = "cd $source && composer $params spip-archive";
        } else {
            $this->runComposerInstall($source);
            $cmd = "cd $source && composer $params archive --format=zip --dir=" . realpath($destination) . " --file=" . $this->getZipName($version);
        }

        passthru($cmd);

        $this->io->writeln("");
        $this->timestampArchive($version);
    }

    /**
     * S’il y a un require dans le composer.json, on fait une installation.
     * permet de cibler SPIP 4.x à venir
     */
    protected function runComposerInstall(string $source): void {
        $json = file_get_contents($source . 'composer.json');
        $json = json_decode($json, true);
        $requires = $json['require'] ?? [];
        $requires = array_filter($requires, function($req) {
            return $req !== 'php' and substr($req, 0, 4) !== 'ext-';
        }, ARRAY_FILTER_USE_KEY);
        if ($requires) {
            if (
                !isset($json['config']['platform']['php'])
                and $php_min = $this->findPhpMin($source)
            ) {
                passthru("cd $source && composer config platform.php $php_min");
            }
            passthru("cd $source && composer install --no-dev --no-cache");
        }
    }

    protected function findPhpMin(string $source): ?string {
        $spip_bootstrap = file_get_contents($source . 'ecrire/inc_version.php');
        $reg = preg_quote("define('_PHP_MIN', '") . '([0-9\.]+)' . preg_quote("');");
        if (preg_match("#$reg#", $spip_bootstrap, $m)) {
            return $this->normalizePhpMin($m[1]);
        }
        return null;
    }

    /** 7.4.0 => 7.4.99 */
    protected function normalizePhpMin(string $version): string {
        $v = explode('.', $version);
        $v[2] = 99;
        return implode('.', $v);
    }

    protected function timestampArchive(string $version) {
        $archiveFile = $this->getBuildFile($version);
        $this->io->section("Change archive date");

        $sourceDir = $this->getSourceDir($version);
        $timestamp = $this->getTimestampLastCommitInSpipProject($sourceDir);
        $date = (new DateTime())->setTimestamp($timestamp)->format("Y-m-d H:i:s");
        $this->io->text("Last commit date : <info>" . $date . "</info>");
        if (file_exists($archiveFile)) {
            touch($archiveFile, $timestamp);
            clearstatcache(true, $archiveFile);
            if ($timestamp == filemtime($archiveFile)) {
                $this->io->text("Date set to : <info>" . $date . "</info>");
            } else {
                $this->io->warning("Date has not been set to : $date" );
            }
            $json = $this->getJson($version, $archiveFile);
            $this->io->section("Json de releases.json");
            $this->io->text($json);

        } else {
            $this->io->warning("No archive found. Can't change archive date.");
        }
    }

    /**
     {
        "version": "4.2.1",
        "released_at": "2023-02-27 17:49:09",
        "announce": [
          {
            "lang": "fr",
            "url": "https://blog.spip.net/Mise-a-jour-critique-de-securite-sortie-de-SPIP-4-2-1-SPIP-4-1-8-SPIP-4-0-10-et.html"
          }
        ],
        "download": [
          {
            "url": "https://files.spip.net/spip/archives/spip-v4.2.1.zip",
            "size": 9916243,
            "sha1": "8a2e4fd1db63f71a454e490cc5a0732934721df7"
          }
        ]
      },
    */
    protected function getJson(string $version, string $archiveFile): string {
        $timestamp = filemtime($archiveFile);
        $date = (new DateTime())->setTimestamp($timestamp)->format("Y-m-d H:i:s");
        if ($version[0] === 'v') {
            $version = substr($version, 1);
        }
        $json = [
            'version' => $version,
            "released_at" => $date,
            'announce' => [[
                'lang' => 'fr',
                'url' => 'https://blog.spip.net/',
            ]],
            'download' => [[
                'url' => 'https://files.spip.net/spip/archives/' . basename($archiveFile),
                'size' => filesize($archiveFile),
                'sha1' => sha1_file($archiveFile),
            ]],
        ];
        return json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

    protected function getTimestampLastCommitInSpipProject(string $sourceDir) : int {
        $timestamp = $this->getTimestampLastCommit($sourceDir);
        foreach ($this->getPluginsPath($sourceDir) as $pluginDir) {
            $t = $this->getTimestampLastCommit($pluginDir);
            if ($t > $timestamp) {
                $timestamp = $t;
            }
        }
        return $timestamp;
    }

    protected function getPluginsPath(string $rootDir) : array {
        $list = [];
        $directories = new \FilesystemIterator($rootDir . '/plugins-dist', \FilesystemIterator::SKIP_DOTS);
        foreach ($directories as $directory) {
            if ($directory->isDir()) {
                $list[] = $directory->getPathname();
            }
        }
        $list[] = $rootDir . '/squelettes-dist';
        sort($list);
        return $list;
    }

    protected function getTimestampLastCommit(string $directory) : int {
        return (int) trim(shell_exec("cd $directory && git log --pretty=tformat:'%ct' -1"));
    }
}
