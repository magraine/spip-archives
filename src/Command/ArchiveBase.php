<?php

namespace Spip\Tools\Archives\Command;

use LogicException;
use Spip\Tools\Archives\ArchiveException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ArchiveBase extends Command {

    protected static $defaultName = 'archive:base';
    protected static $defaultDescription = 'Génère une archive d’un SPIP';

    /** @var SymfonyStyle */
    protected $io;

    /** @var string */
    protected $componentDir = 'base/';

    /** @var string */
    private $buildDir = 'build/';
    /** @var string */
    private $sourceDir = 'sources/';


    protected function configure() {
        $this
            ->setDescription(static::$defaultDescription)
            ->addArgument('version', InputOption::VALUE_REQUIRED, 'Branche ou tag', 'master')
            ->addOption('no-download', null, InputOption::VALUE_NONE, 'Ne pas relancer composer:install si cette source existe déjà.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $io = $this->io = new SymfonyStyle($input, $output);
        $io->title($this->getDescription());

        $version = $this->getAskedVersion($input);
        $noDownload = $input->getOption('no-download');
        $interactive = $input->isInteractive();

        if (!$this->download($version, $noDownload, $interactive)) {
            $io->error(sprintf("Echec de download de la version '%s'", $version));
            return Command::FAILURE;
        }
        $this->build($version, $interactive);

        return Command::SUCCESS;
    }

    protected function download(string $version, bool $noDownload = false, bool $interactive = true): bool {
        throw new LogicException('You must override the download() method in the concrete command class.');
    }

    protected function build(string $version, bool $interactive = true) {
        throw new LogicException('You must override the build() method in the concrete command class.');
    }

    protected function getAskedVersion(InputInterface $input) : string {
        $version = $input->getArgument('version');
        if ($version !== 'master') {
            if (0 === strpos($version, 'spip-')) {
                $_version = substr($version, 5);
                $this->io->text("Transform <comment>$version</comment> argument to : <comment>$_version</comment>");
                $version = $_version;
            }
            // Tag sans le 'v' ?
            if (
                $version[0] !== 'v' 
                && count(explode('.', $version)) > 2 
                // branches anciennes assez spécifiques...
                && !in_array($version, ['1.9.1', '1.9.2'])
            ) {
                $this->io->text("Transform <comment>$version</comment> argument to : <comment>v$version</comment>");
                $version = 'v' . $version;
            }
        }
        return $version;
    }

    protected function createDirectory(string $directory) {
        if (!is_dir($directory)) {
            if (!mkdir($directory, 0777, true)) {
                throw new ArchiveException("Répertoire $directory non créé");
            }
        }
    }

    protected function unlink($file) {
        if (is_file($file)) {
            if (!unlink($file)) {
                throw new ArchiveException("Echec de suppression de $file");
            }
        }
    }

    protected function emptyDirectory($directory) {
        $directory = rtrim(trim($directory), '/');
        if (
            $directory
            and $directory[0] !== '/'
            and (strpos($directory, '..') === false)
            and is_dir($directory)
        ) {
            passthru("rm -rf $directory/*");
        } else {
            throw new ArchiveException("On ne peut vider $directory");
        }
    }

    protected function copyTemplate(string $fileTemplate, string $destination, string $version, bool $interactive = true) {
        $content = file_get_contents($fileTemplate);
        $replace = [
            '@buildDir@' => realpath($this->getBuildDir($version)),
            '@fileName@' => $this->getZipName($version),
            // Composer download
            '@spipRequire@' => $this->getSpipRequire($version),
            '@templateDir@' => rtrim($this->getTemplateDir(), '/'),
            '@repositoryDir@' => 'https://gitlab.com/magraine/spip-archives/-/raw/master/templates/composer/',
            '@params@' => $interactive ? '' : ' --no-interaction',
        ];
        $content = str_replace(array_keys($replace), $replace, $content);
        if (!file_put_contents($destination, $content)) {
            throw new ArchiveException("Échec création de $fileTemplate dans $destination");
        }
    }

    protected function getSourceDir(string $version) : string {
        return $this->sourceDir . $this->componentDir . $version . '/';
    }

    protected function getBuildDir(string $version) : string {
        return $this->buildDir . $this->componentDir;
    }

    protected function getBuildFile(string $version) : string {
        return $this->getBuildDir($version) . $this->getZipName($version) . '.zip';
    }

    protected function getTemplateDir() : string {
        return realpath(dirname(dirname(__DIR__))) . '/templates/' . $this->componentDir;
    }

    protected function getPathTemplateComposer() : string {
        return $this->getTemplateDir() . 'composer.json';
    }

    protected function getZipName(string $version) : string {
        if ($version === 'master') {
            return 'spip-master';
        }
        if (0 === stripos($version, 'spip-')) {
            $version = substr($version, 5);
        }
        if ($version[0] === 'v') {
            $version = substr($version, 1);
        }
        $v = explode('.', $version);
        if (count($v) >= 3) {
            return "spip-v" . $version;
        }
        return 'spip-' . $version;
    }

    protected function getSpipRequire(string $version) : string {
        if ($version === 'master') {
            return 'dev-master';
        }
        // branche : "3.2.x-dev"
        // tag : "3.2.7"
        if (0 === strpos($version, 'spip-')) {
            $version = substr($version, 5);
        }
        if ($version[0] === 'v') {
            $version = substr($version, 1);
        }
        $v = explode('.', $version);
        if (count($v) >= 3) {
            // 3.2.7
            return $version;
        }
        // 3.2
        return $version . '.x-dev';
    }
}
