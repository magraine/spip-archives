<?php

namespace Spip\Tools\Archives\Command;

use Spip\Tools\Archives\ArchiveException;

class ArchiveComposer extends ArchiveBase {

    protected static $defaultName = 'archive:composer';
    protected static $defaultDescription = 'Génère une archive d’un SPIP (avec Composer)';

    protected $componentDir = 'composer/';

    protected function download(string $version, bool $noDownload = false, bool $interactive = true): bool {
        $source = $this->getSourceDir($version);
        $destination = $this->getBuildDir($version);

        $this->io->section("Composer install $version in $source");
        if ($noDownload) {
            if (
                is_file($source . 'composer.json')
                and is_file($source . 'vendor/autoload.php')
            ) {
                $this->io->text("! <comment>Composer install ignoré.</comment>");
                return true;
            }
        }

        $composer_file = $source . 'composer.json';
        $composer_lock_file = $source . 'composer.lock';
        $this->unlink($composer_file);
        $this->unlink($composer_lock_file);

        $this->createDirectory($source);
        // j’arrive pas à expliquer pour le moment qu’une fois sur 2 
        // il n’installe pas squelettes-dist/ si on conserve les fichiers...
        $this->emptyDirectory($source);
        // realpath test réellement l’existence du répertoire
        // et on le calcule dans le template composer...
        $this->createDirectory($destination);

        $template = $this->getPathTemplateComposer();
        $this->copyTemplate($template, $composer_file, $version, $interactive);

        $params = $interactive ? '' : ' --no-interaction';
        $cmd = "cd $source && composer $params install --no-dev --no-cache";
        passthru($cmd);
        // TODO: Vérifier la version téléchargée
        return true;
    }

    protected function build(string $version, bool $interactive = true) {
        
        $destination = $this->getBuildDir($version);
        $source = $this->getSourceDir($version);

        $this->io->section("Build $version in $destination");

        $params = $interactive ? '' : ' --no-interaction';
        $cmd = "cd $source && composer $params spip-archive";
        passthru($cmd);
    }

}
